//GUARDAR DATOS CLIENTE
function GuardarCliente(req, res) {  
        const nombre = req.body.nombre;
        const apellidos = req.body.nombre;
        const cedula = req.body.cedula;
        const telefono = req.body.telefono;
        const direccion = req.body.direccion;
        
        if (!nombre || !apellidos || !cedula || !telefono || !direccion) {
            console.log('Ingrese datos del cliente')
            return res.render('crearcliente', {
                alert: 'Ingrese datos del cliente'
            });
        }
        conexion.query('SELECT * FROM cliente WHERE Nombre = ? AND Apellidos = ? AND Cedula = ? AND Telefono = ? AND Direccion=? ', [nombre, apellidos, cedula, telefono, direccion], (error, result) => {

            if (result == 0) {
                conexion.query('INSERT INTO cliente SET ?', { Nombre: nombre, Cedula: cedula, Apellidos: apellidos, Telefono: telefono, Direccion: direccion }, (error, results) => {
                    if (error) {
                        console.log(error);
                    } else { 
                        res.redirect('/ListaCliente');
                    }
                });

            } else {
                console.log('Cliente ya se encuentra registrado')
                res.render('crearCliente', {
                    alert: 'Cliente ya se encuentra registrado'
                });
            }
        })
}
